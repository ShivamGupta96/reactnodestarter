var express = require('express');

var app = express();

app.use(express.static('./public'));
app.use(express.static('./node_modules/bootstrap/dist'));

app.listen(3031, () => {
    console.log("Server started at 'http://localhost:3031'");
});
